    $( document ).ready(function() {

        $("#form-reservation").on('submit',function(){
            var condition = [];
            var chk = false;
            var chkSub = false;
            var checkType = ['Name', 'Lastname', 'E-Mail', 'Mobile Phone', 'Address', 'Room Type', 'Room Reqirement', 'Date Form', 'Date End' ];

            condition.push($("#name").val());
            condition.push($("#lastname").val());
            condition.push($("#email").val());
            condition.push($("#mobile").val());
            condition.push($("#address").val());
            condition.push($("#room_type :selected").val());
            condition.push($("#room_require :selected").val());
            condition.push($("#dateForm").val());
            condition.push($("#dateEnd").val());
            
            $.each(condition, function(indexs, value){
                if( indexs == 0 || indexs == 1 || indexs == 2 || indexs == 3 || indexs == 4 || indexs == 7 || indexs == 8 ) {
                    if( value == ""){
                        alert("Please Insert "+checkType[indexs]);
                        chk = true;
                    } 
                } else if( indexs == 5 || indexs == 6 ) {
                    if( value == 0){
                        alert("Please Choose "+checkType[indexs]);
                        chk = true;
                    } 
                }

                if( chk ) {
                    return false;
                }
            });

            if( chk ) {
                return false;
            } else{
                return true;
            }
             
        });

        $('#slider').bxSlider({
            pager: false,
            nextText : '<i class="fa fa-caret-left fa-2x" aria-hidden="true"></i>',
            prevText : '<i class="fa fa-caret-right fa-2x" aria-hidden="true"></i>',
            nextSelector : '.buttonPrev',
            prevSelector : '.buttonNext',
        });

        $("#custom-main-gallery-room").bxSlider({
            controls: false
        });

        $("#custom-main-gallery-surround").bxSlider({
            controls: false
        });

        $("#custom-main-gallery-lobby").bxSlider({
            controls: false
        });

         $("#custom-main-gallery-beach").bxSlider({
            controls: false
        });

        $("[data-fancybox]").fancybox({
            iframe : {
                css : {
                    width : '668px',
                    height : '707px'
                }
            },
        });

        $("#custom-main-gallery a").fancybox({
            iframe : {
                css : {
                    width : '668px',
                    height : '447px'
                }
            }
        });

        $(".entry-toggle-term").on('click',function(){
            if ( $( ".entry-terms" ).is( ":hidden" ) ) {
                $( ".entry-terms" ).slideDown( "slow" );
            } else {
                return false;
            }
        });

        $(".closed-button").on('click', function(){
            if ( !$( ".entry-terms" ).is( ":hidden" ) ) {
                $( ".entry-terms" ).slideUp( "slow" );
            } else {
                return false;
            }
        });

        $('#fade1').cycle({ 
            fx:'fade', 
            speed:  3000 
        });
        
        $('#fade2').cycle({ 
            fx:'fade', 
            speed:  4000 
        });
        
        $('#fade3').cycle({ 
            fx:'fade', 
            speed:  5000 
        });
        
        $('#fade4').cycle({ 
            fx:'fade', 
            speed:  6000 
        });
        
        $('#fade5').cycle({
            fx:'fade', 
            speed:  7000 
        });

        $('#left-bot').cycle({
            fx:'fade', 
            speed:  4000 
        });

        $('#right-bot').cycle({
            fx:'fade', 
            speed:  5000 
        });
        
        $(".entry-page-gallery li").on('click', function(){
            $(".entry-page-gallery li").removeClass('active');
            $(this).addClass('active');
            $(".event-gallery").removeClass('active');
            $( "#"+$(this).attr('data-id') ).addClass('active');
        });

        var dateFormat = "mm/dd/yy",
        from = $( "#dateForm" ) .datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1
        }).on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
        }), 
        to = $( "#dateEnd" ).datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1
        }) .on( "change", function() {
            from.datepicker( "option", "maxDate", getDate( this ) );
        });
    });

    function getDate( element ) {
        var date;
        try {
            date = $.datepicker.parseDate( dateFormat, element.value );
        } catch( error ) {
            date = null;
        }
        return date;
    }

    function setOnload(){
        $(".event-gallery").removeClass('active');
        $( "#room").addClass('active');
    }
